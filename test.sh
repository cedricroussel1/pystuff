#!/bin/sh
# stop on first error
set -ev

#pycodestyle --ignore=W605 src/
#pycodestyle tests/

#pyflakes src/
#pyflakes tests/

PYTHONPATH=src python3 -m nose -s --with-coverage --cover-package=catcli
