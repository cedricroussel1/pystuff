from typing import List

data: List[str] = ['Albert Einstein',
                   'Marie Curie',
                   'Isaac Newton']


def test_lambda():
    assert ['Marie Curie',
            'Albert Einstein',
            'Isaac Newton'] == sorted(data, key=lambda n: n.split()[1])
