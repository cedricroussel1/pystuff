# default values are evaluated once when module is loaded
# not on every function call


import unit_testing.default_values.default_values as m


def test_get_values_no_mutation():
    assert (0, 0) == m.get_values()


def test_inc_with_default_eq_one():
    m.mutable_default = 42
    assert (0, 42) == m.get_values()
