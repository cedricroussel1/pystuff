import socket
import unittest.mock as mock
from typing import List

import unit_testing.resolver.resolver as res


def test_resolver_caching():
    r = res.Resolver()
    with mock.patch.object(socket, 'gethostbyname', wraps=socket.gethostbyname) as monkey:
        print(r("gitlab.com"))
        print(r("google.com"))
        print(r("gitlab.com"))
        print(r("google.com"))
        monkey.assert_any_call('gitlab.com')
        assert monkey.call_count == 2
        assert r.has_host("gitlab.com")


def test_sequence_class():
    assert isinstance(res.sequence_class(True)("seq"), list)
    assert isinstance(res.sequence_class(False)("seq"), tuple)


