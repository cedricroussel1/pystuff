# default values are evaluated once when module is loaded
# not on every function call

mutable_default: int = 0


def get_values(b=mutable_default):
    return b, mutable_default
