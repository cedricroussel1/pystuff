# modules are only executed once, when first imported
# module initialisation order is well defined
# practical singleton

_registry = []
print("loading registry")


def register(name):
    _registry.append(name)


def registered_names():
    return iter(_registry)
