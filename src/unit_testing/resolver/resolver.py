import socket
from typing import Dict, Optional


# A callable object (__call__) can be invoked like regular function

class Resolver:
    def __init__(self):
        self._cache: Dict[str, str] = {}

    def __call__(self, host: str) -> Optional[str]:
        if host not in self._cache:
            print("cache miss")
            self._cache[host] = socket.gethostbyname(host)
        else:
            print("cache hit")
        return self._cache.get(host)

    def has_host(self, host: str) -> bool:
        return host in self._cache


def sequence_class(mutable):
    return list if mutable else tuple
