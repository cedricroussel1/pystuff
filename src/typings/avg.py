def avg(a, b, c):
    return (a + b + c) / 3


def typed_avg(a: int, b: int, c: int = 0) -> float:
    return (a + b + c) / 3


foo: int = 32

# print(avg(1, 5, 7))
# print(avg(1, '5', 7))
# print(typed_avg(1, '5', 7))
print(typed_avg(1, 5, 7))
