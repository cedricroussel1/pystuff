import random
from typing import Dict, Callable


class Sensor:
    _OFFSET: int = 16

    def sample_pressure(self):
        pressure_telemetry_value = self.simulate_pressure()
        return Sensor._OFFSET + pressure_telemetry_value

    @staticmethod
    def simulate_pressure() -> float:
        pressure_telemetry_value = 6 * random.random() * random.random()
        return pressure_telemetry_value


class Alarm:

    def __init__(self, sensor: Sensor):
        self._low_pressure_threshold: int = 17
        self._high_pressure_threshold: int = 21
        self._sensor: Sensor = sensor or Sensor()
        self._is_alarm_on: bool = False

    def check(self) -> None:
        pressure = self._sensor.sample_pressure()
        if pressure < self._low_pressure_threshold \
                or self._high_pressure_threshold < pressure:
            self._is_alarm_on = True

    def is_alarm_on(self) -> bool:
        return self._is_alarm_on


def test_alarm_type_checking():
    # alarm = Alarm(32)
    alarm = Alarm(Sensor())
    assert not alarm.is_alarm_on() == 32


x: Dict[str, float] = {'field': 2.0}


# Add default value for an argument after the type annotation
def f(num1: int, my_float: float = 3.5) -> float:
    return num1 + my_float


# This is how you annotate a callable (function) value
fn: Callable[[int, float], float] = f
