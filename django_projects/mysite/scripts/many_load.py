import csv

from unesco.models import Site, State, Iso, Region, Category


def nfloat(s):
    if (s == ""):
        return 0;
    return float(s);


def run():
    fhand = open('unesco/whc-sites-2018-clean.csv')
    reader = csv.reader(fhand)
    next(reader)  # Advance past the header

    Site.objects.all().delete()
    Category.objects.all().delete()
    Region.objects.all().delete()
    Iso.objects.all().delete()
    State.objects.all().delete()

    for row in reader:
        print(row)

        c, created = Category.objects.get_or_create(name=row[7])
        s, created = State.objects.get_or_create(name=row[8])
        r, created = Region.objects.get_or_create(name=row[9])
        i, created = Iso.objects.get_or_create(name=row[10])

        site = Site(name=row[0],
                    description=row[1],
                    justification=row[2],
                    year=row[3],
                    longitude=nfloat(row[4]),
                    latitude=nfloat(row[5]),
                    area_hectares=nfloat(row[6]),
                    category=c,
                    state=s,
                    region=r,
                    iso=i
                    )
        site.save()
