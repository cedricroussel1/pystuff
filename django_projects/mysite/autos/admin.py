from autos.models import Make, Auto
from django.contrib import admin

# Register your models here.

admin.site.register(Make)
admin.site.register(Auto)
