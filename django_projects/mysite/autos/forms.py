from autos.models import Make
from django.forms import ModelForm


# Create the form class.
class MakeForm(ModelForm):
    class Meta:
        model = Make
        fields = '__all__'
