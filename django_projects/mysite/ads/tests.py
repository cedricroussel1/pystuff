from django.contrib.auth.models import User
from django.db.models import QuerySet
from django.test import TestCase
from django.urls import reverse

from .models import Ad


class AdViewTests(TestCase):
    def test_add_view_with_no_ads_in_db(self):
        response = self.client.get(reverse('ads:all'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No ads are available.")
        self.assertQuerysetEqual(response.context['ad_list'], [])

    def test_add_view_with_an_ads_in_db(self):
        user = User.objects.create_user(username='testuser', password='12345')
        ad=Ad.objects.create(owner_id=user.id, text="foo", title="my ad title")
        response = self.client.get(reverse('ads:all'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "my ad title")
        self.assertEqual(response.context['ad_list'][0],ad)
