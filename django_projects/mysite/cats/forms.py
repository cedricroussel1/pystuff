from cats.models import Breed
from django.forms import ModelForm


# Create the form class.
class BreedForm(ModelForm):
    class Meta:
        model = Breed
        fields = '__all__'
