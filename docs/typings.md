- type hints are used by pycharm
- python interpreter ignore them
- type errors will show as warning by default (but can change setting to error)
- https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html

